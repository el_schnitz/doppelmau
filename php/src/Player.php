<?php
/**
 * @Entity @Table(name="players")
 **/
class Player
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    /** @Column(type="string") */
    protected $name;

    protected $start_num;

    /** admin */
    protected $host;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getStartNum()
    {
        return $this->start_num;
    }

    /**
     * @return mixed
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param mixed $start_num
     */
    public function setStartNum($start_num)
    {
        $this->start_num = $start_num;
    }
}