<?php
/**
 * @Entity @Table(name="game")
 **/
class Game
{
    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;
    /** @Column(type="string") */
    protected $status;

    protected $requested_shape;

    /** admin */
    protected $draw_counter;

    protected $turn;

    protected $info;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getRequested_shape()
    {
        return $this->requested_shape;
    }

    /**
     * @return mixed
     */
    public function getDraw_counter()
    {
        return $this->draw_counter;
    }

    /**
     * @return mixed
     */
    public function getTurn()
    {
        return $this->turn;
    }

    /**
     * @return mixed
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * @param mixed $requested_shape
     */

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function setRequested_shape($requested_shape)
    {
        $this->requested_shape = $requested_shape;
    }

    /**
     * @param mixed $draw_counter
     */
    public function setDrawCounter($draw_counter)
    {
        $this->draw_counter = $draw_counter;
    }

    /**
     * @param mixed $turn
     */
    public function setTurn($turn)
    {
        $this->turn = $turn;
    }

    /**
     * @param mixed $info
     */
    public function setInfo($info)
    {
        $this->info = $info;
    }
}