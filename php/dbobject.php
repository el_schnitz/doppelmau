<?php

class DBObject
{
    /**
     * Liest ein einzelnes Objekt aus der Datenbank aus
     * @param string $table
     * @param string $column
     * @param string $key
     * @return array wenn das Objekt in der DB gefunden wurde, sonst false
     */
    static function load($table, $column, $key)
    {
        $connection = DB::get_connection();
        $sql = $connection->prepare("SELECT * FROM $table WHERE $column = ?");
		$success = $sql->execute(array($key));

        if($success)
        {
            return $sql->fetch(PDO::FETCH_ASSOC);
        }
        return false;
    }

    function save()
    {

    }
}