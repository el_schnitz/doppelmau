<?php
class DB{
	private static $database;
	private $connection;

	private function __construct()
	{
		$config = parse_ini_file("config.ini");
		$this->connection = new PDO("mysql:host=" . $config["host"] . ";dbname=" . $config["database"], $config["username"], $config["password"]);
		
		//damit bei Fehlern in der Abfrage eine Exception geworfen wird
		$this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		//damit Zahlen(INT, FLOAT) als Zahlen und nicht Strings ausgelesen werden
		$this->connection->setAttribute(PDO::ATTR_STRINGIFY_FETCHES, false);
		$this->connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	}

	function __destruct(){
		$this->connection = NULL;
	}

	/**
	 * gibt Datenbankverbindung zurück
	 */
	public static function get_connection()
	{
		if(self::$database === null)
		{
			self::$database = new DB();
		}
		return self::$database->connection;
	}

	/**
	 * gibt Datenbank (für eigene Funktionen) zurück
	 */
	public static function get_db()
	{
		if(self::$database === null)
		{
			self::$database = new DB();
		}
		return self::$database;
	}


	//Verbindung zur Datenbank
	public static $conn;
	
	
	/*
	* Stellt Verbindung zur Datenbank her, muss einmal am Anfang aufgerufen werden
	*/
	public static function connect(){
		$config = parse_ini_file("config.ini");
		self::$conn = new PDO("mysql:host=localhost;dbname=" . $config["mysql.database"], $config["mysql.username"], $config["mysql.password"]);
		
		//damit bei Fehlern in der Abfrage eine Exception geworfen wird
		self::$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		//damit Zahlen(INT, FLOAT) als Zahlen und nicht Strings ausgelesen werden
		self::$conn->setAttribute(PDO::ATTR_STRINGIFY_FETCHES, false);
		self::$conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		
		return "Datenbankverbindung erfolgreich hergestellt";
	}
	
	/*public static function insert($table_name, $object){
		$key_val = get_object_vars($object);
		$key_array = array_keys($key_val);
		$keys = implode(", ", $key_array );
		$vals = ":" . implode(", :", $key_array );
		
		$sql = self::$conn->prepare("INSERT INTO $table_name ($keys) VALUES ($vals)");
		
		$success = $sql->execute($key_val);
		
		if(!$success){
			$return = $sql->errorInfo();
			$return[] = $sql->queryString;
		} else {
			$return = "SUCCESS";
		}
		
		return $return;
	}*/
	
	/**
	 * Gibt Daten aus einer Tabelle als Array von assoziativen Arrays zurück
	 * @return array
	 */
	public function select($table_name, $where = null)
	{
		if($where != null){
			$where = " WHERE " . $where;
		}
		$sql = $this->connection->prepare("SELECT * FROM $table_name" . $where);
		
		$success = $sql->execute();
		
		if(!$success){
			$return = $sql->errorInfo();
			$return[] = $sql->queryString;
		} else {
			$return = $sql->fetchAll(PDO::FETCH_ASSOC);
		}
		
		return $return;
	}

	/**
	 * Gibt erstes Element aus einer Tabelle als assoziatives Array zurück
	 * @return array
	 */
	public function select_first($table_name, $where = null)
	{
		$all = $this->select($table_name, $where);
		if(sizeof($all) === 0){
			return NULL;
		}
		return $all[0];
	}
	
	/**
	 * aktualisiert eine Spalte in einer Tabelle
	 */
	public function update($table_name, $column, $value, $where = null)
	{
		if($where != null){
			$where = " WHERE " . $where;
		}
		
		$sql = $this->connection->prepare("UPDATE $table_name SET $column = ?" . $where);
		
		$success = $sql->execute(array($value));
		
		if(!$success){
			$return = $sql->errorInfo();
			$return[] = $sql->queryString;
		} else {
			$return = $sql->numRows();
		}
		
		return $return;
	}
	
	/*
	* Gibt die Anzahl der Einträge in einer Tabelle zurück
	*/
	/*public static function count_rows($table_name, $where = null){
		if($where != null){
			$where = " WHERE " . $where;
		}
		$sql = self::$conn->prepare("SELECT COUNT(1) FROM $table_name" . $where);
		
		$success = $sql->execute();
		
		if(!$success){
			$return = $sql->errorInfo();
			$return[] = $sql->queryString;
		} else {
			$return = intval($sql->fetch(PDO::FETCH_NUM)[0]);
		}
		
		return $return;
	}*/
	
}

?>