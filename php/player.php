<?php

class Player extends DBObject
{
    public $name;
    public $start_num;
    public $mau_gesagt;
    public $draw_counter;

    function __construct($session_id)
    {
        $properties = parent::load('players', 'session_id', $session_id);
        if($properties != false)
        {
            $this->name = $properties['name'];
            $this->start_num = $properties['start_num'];
        }
    }

    /**
     * Zieht eine oder meherere Karten (abhängig von draw_counter)
     * 
     */
    static function draw_cards($amount = NULL)
    {
        $response = array(
            "success" => false,
            "shuffeled" => false,
            "amount" => 0
        );
        //=================================  CHECKS  ====================================

        //Ist Spieler an der Reihe?
        if(Spiel::$config['turn'] !== Spiel::$this_player['start_num']){
            $response['message'] = "not_your_turn";
            return $response;
        }

        //Hat Spieler bereits gespielt/gezogen
        // Wenn amount gegeben ist, wird ziehen erzwungen (z.b. für Strafkarten)
        if(Spiel::$config['turn_stage'] != 0 && $amount === NULL)
        {
            $response["message"] = "already_played";
            return $response;
        }

        //==============================  AUSFÜHRUNG  =================================

        //Standard: draw_counter oder 1 Karte
        if($amount === NULL){
            $amount = Spiel::$config['draw_counter'] !== 0 ? Spiel::$config['draw_counter'] : 1;
        }
        
        $db = DB::get_connection();

        for($i = 0; $i < $amount; $i++){
            $sql = $db->prepare(
                "UPDATE cards SET status = '" . Card::IN_HAND .
                "', on_hand = '" . Spiel::$this_player["id"] . 
                "' WHERE status = '0' ORDER BY RAND() LIMIT 1");
            $success = $sql->execute();

            //
            if($success){
                if($sql->rowCount() === 0){
                    //Keine Karte gefunden, mischen und neu ziehen
                    Spiel::shuffle_cards();
                    $response['shuffeled'] = true;
                    $i--;
                }
                $response['amount'] += 1;
                $response['success'] = $success;
            }
            
        }
        if($response['amount'] > 0)
        {
            $sql = $db->prepare("UPDATE game SET turn_stage = ?, draw_counter = ?");
            $sql->execute(array(Spiel::TS_DRAWN, 0));
        }
        return $response;
    }

    /**
     * Prüft ob eine Karte gelegt werden kann und legt sie
     * @param string $karte z.B. "PiX" oder "KaA"
     */
    function play_card($card_string)
    {
        $response = array(
            "success" => false
        );

        //Parameter überprüfen
        if(gettype($card_string) !== 'string' || strlen($card_string) !==  3){
            $response['message'] = "card_not_valid";
            return $response;
        }
        $card = array(
            "shape" => substr($card_string, 0, 2),
            "value" => substr($card_string, 2, 1)
        );

        //=================================  CHECKS  ====================================

        //Ist Spieler an der Reihe?
        if(Spiel::$config['turn'] !== Spiel::$this_player['start_num']){
            $response['message'] = "not_your_turn";
            return $response;
        }

        //Hat Spieler bereits gezogen/gespielt
        if(Spiel::$config['turn_stage'] !== 0){
            $response['message'] = "already_played";
            Spiel::set_info(Spiel::$this_player['name'] . ", du hast bereits eine Karte gezogen oder gelegt. Bitte beende den Zug.");
            return $response;
        }

        // Passt die Karte?
        if(!Spiel::check_card($card)){
            $response['message'] = "no_match";
            Spiel::set_info(Spiel::$this_player['name'] . ", diese Karte passt nicht. Bitte lege eine andere oder ziehe eine Karte!");
            return $response;
        }

        // Karten müssen gezogen werden
        if(Spiel::$config['draw_counter'] != 0)
        {
            if(!($card['value'] === '7' || $card['value'] === '8'))
            {
                Spiel::set_info(Spiel::$this_player['name'] . ", du musst entweder " . Spiel::$config['draw_counter'] . " Karte(n) ziehen oder eine passende 7 oder 8 legen!");
                $response['message'] = "must_draw";
                return $response;
            }
        }

        //==============================  AUSFÜHRUNG  =================================
        
        $db = DB::get_connection();
        //Oberste Karte in Ablagestapel
        $sql = $db->prepare("UPDATE cards SET status = ? WHERE status = ?");
        $success = $sql->execute(array(Card::IN_GRAVEYARD, Card::ON_TOP));
        if(!$success)
        {
            $response['error'] = $sql->errorInfo();
            $response['query'] = $sql->queryString;
            return $response;
        }

        //Neue Karte als oberste Karte festlegen
        $sql = $db->prepare("UPDATE cards SET status = ?, on_hand = NULL WHERE form = ? AND value = ?");
        $success = $sql->execute(array(Card::ON_TOP, $card['shape'], $card['value']));

        if(!$success)
        {
            $response['error'] = $sql->errorInfo();
            $response['query'] = $sql->queryString;
            return $response;
        }

        
        //=============================  SPEZIALFÄLLE  =================================
        if($card['value'] === '7'){
            Spiel::$config['draw_counter'] += 2;
            
            $sql = $db->prepare("UPDATE game SET draw_counter = ?");
            $sql->execute(array(Spiel::$config['draw_counter']));
        }
        if($card['value'] === '8'){
            Spiel::$config['draw_counter'] += 1;
            
            $sql = $db->prepare("UPDATE game SET draw_counter = ?");
            $sql->execute(array(Spiel::$config['draw_counter']));
        }
        
        
        //Karte wurde erfolgreich gespielt, turn_stage ändern
        $sql = $db->prepare("UPDATE game SET turn_stage = ?, requested_shape = ?");
        if($card['value'] === 'B'){
            $success = $sql->execute(array(Spiel::TS_HAS_TO_WISH, NULL));
        } else {
            $success = $sql->execute(array(Spiel::TS_PLAYED, NULL));
        }

        if(!$success)
        {
            $response['error'] = $sql->errorInfo();
            $response['query'] = $sql->queryString;
            return $response;
        }


        $response['success'] = true;
        return $response;
    }

     function form_wuenschen($form){
        $db = DB::get_connection();
        $sql = $db->prepare("UPDATE game SET requested_form = ?");
        $success = $sql->execute(array($form));

        if(!$success){
            return false;
        }
        return true;
     }

     function mau_sagen(){}
     /**
      * mau erhöhen
      */

     function end_turn()
     {
        $response = array(
            "success" => false
        );

        // TODO Checks durchführen
        if(Spiel::$config['turn_stage'] === 0){
            //Spieler hat noch nicht gespielt
            return $response;
        }

        $target_mau = max(0, 2 - Player::card_count());

        if(Spiel::$config['mau_counter'] < $target_mau)
        {
            Spiel::set_info(Spiel::$this_player['name'] . ', du hast MAU vergessen. Das gibt eine Strafkarte!');
            $response['message'] = 'too_little_mau';
            Player::draw_cards(1);
        }

        if(Spiel::$config['mau_counter'] > $target_mau)
        {
            Spiel::set_info(Spiel::$this_player['name'] . ', du darfst hier nicht MAU sagen. Das gibt eine Strafkarte!');
            $response['message'] = 'too_much_mau';
            Player::draw_cards(1);
        }

        $response["success"] = true;
        $response["next_player"] = Spiel::next_player();
        return $response;
     }

     function card_count($id = NULL)
     {  
         if($id === NULL) $id = Spiel::$this_player['id'];

         $db = DB::get_connection();
         $sql = $db->prepare('SELECT COUNT(*) FROM cards WHERE on_hand = ?');
         $sql->execute(array($id));

         return $sql->fetch(PDO::FETCH_COLUMN, 0);
     }
}