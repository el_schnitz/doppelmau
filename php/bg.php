<?php
    session_start();
    require('db.php');
    require('dbobject.php');
    require("spiel.php");
    require("player.php");
    require("card.php");
    $response = array();
    
    $func = filter_input(INPUT_POST, "func");
    
    $ini = parse_ini_file("config.ini", true);
    $host = $ini["database"]["host"];
    $user = $ini["database"]["username"];
    $password = $ini["database"]["password"];
    $database = $ini["database"]["database"];
    
    $con = new mysqli($host, $user, $password, $database);
    if ($con->connect_error)
        die("Verbindung fehlgeschlagen: " . $con->connect_error);
    else
    {
        //Spiel, Spieler und oberste Karte laden
        Spiel::load_data();

        switch($func)
        {
            case "check":
                $player_name = filter_input(INPUT_POST, "player_name");
                
                //holt gewünschte Form, zu ziehende Karten, wer am Zug ist, und anzuzeigende Info aus der DB
                //außerdem die Karten auf der Hand des lokalen Spielers und die aufgedeckte Karte
                $result = $con->query("SELECT * FROM game");
                if($result->num_rows == 1)
                {
                    $row = $result->fetch_assoc();
                    $response['requested_shape'] = $row['requested_shape'];
                    $response['draw_counter'] = $row['draw_counter'];
                    $response['turn'] = $row['turn'];
                    $response['info'] = $row['info'];
                    
                    //Karten des lokalen Spielers auslesen
                    $self_cards = array();
                    $result = $con->query("SELECT id FROM players WHERE name='$player_name'");
                    if($result->num_rows == 1)
                    {
                        $row = $result->fetch_assoc();
                        $self_cards_result = $con->query("SELECT * FROM cards WHERE on_hand=".$row["id"]);
                        for($i = 0; $i < $self_cards_result->num_rows; $i++)
                        {
                            $row_self_cards = $self_cards_result->fetch_assoc();
                            $formvalue = $row_self_cards["form"].$row_self_cards["value"];
                            array_push($self_cards, $formvalue);
                        }
                        $response['self_cards'] = $self_cards;

                        //oberste Karte auslesen
                        $open_card_result = $con->query("SELECT * FROM cards WHERE status=2");
                        $open_card_row = $open_card_result->fetch_assoc();
                        $response['open_card'] = $open_card_row["form"].$open_card_row["value"];
                    }
                }
                else
                    $con->query("UPDATE game SET info='Willkommen bei DoppelMau!'");
                
                //holt Anzahl Spieler
                $player_result = $con->query("SELECT * FROM players");
                $response['pcount'] = $player_result->num_rows;
                
                //holt Name, Startnummer, Hostwert und Kartenanzahl jedes Spielers
                $response['players'] = array();
                for($i = 0; $i < $player_result->num_rows; $i++)
                {
                    $row = $player_result->fetch_assoc();
                    
                    $response['players'][] = array(
                        'name' => $row["name"], 
                        'start_num' => $row["start_num"], 
                        'host' => $row["host"],
                        'num_cards' => Player::card_count($row['id'])
                    );
                }
                

                //==================== AM I HOST ============================
                
                //nimmt Hostwert für den übergebenen Spielernamen aus der players-Tabelle
                $result = $con->query("SELECT host FROM players WHERE name='$player_name'");
                $row = $result->fetch_assoc();
                if($row["host"] == 1)
                    $response['host'] = true;
                else
                    $response['host'] = false;

                //============ GAME STARTED, TURN STAGE, MAU =====================

                //sucht in game-Tabelle nach einem Eintrag (und damit, ob das Spiel gestartet wurde)
                $result = $con->query("SELECT * FROM game");
                if($result->num_rows > 0)
                {
                    $row = $result->fetch_assoc();
                    $response['game_started'] = true;
                    $response['turn_stage'] = $row["turn_stage"];
                    $response['mau_counter'] = $row['mau_counter'];
                } else
                {
                    $response['game_started'] = false;
                    $response['turn_stage'] = false;
                    $response['mau_counter'] = 0;   
                }
                    

                //===================== MY TURN ===================================

                $result = $con->query("SELECT * FROM players p, game g WHERE p.name='$player_name' AND p.start_num = g.turn");
                if ($result->num_rows > 0)
                    $response['my_turn'] = true;
                else
                    $response['my_turn'] = false;

                //===================== PLAYER IN DB =================================
                $result = $con->query("SELECT * FROM players WHERE name='$player_name'");
                if ($result->num_rows > 0)
                    $response['player_in_db'] = true;
                else
                    $response['player_in_db'] = false;

                break;
        }
    }
    
    //Ausgabe des Antwort-Arrays (ist dadurch der Rückgabewert, der aufrufenden Ajax-Funktion
    header('Content-Type: application/json');
    echo json_encode($response);
?>