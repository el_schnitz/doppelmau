<?php

error_reporting(0);

$ini = parse_ini_file("config.ini", true);
$host = $ini["database"]["host"];
$user = $ini["database"]["username"];
$password = $ini["database"]["password"];
$database = $ini["database"]["database"];

$con = new mysqli($host, $user, $password, $database);

if($con->connect_error or true){
  $root_user = $ini["root"]["username"];
  $root_password = $ini["root"]["password"];
  $con = new mysqli($host, $root_user, $root_password);

  //Datenbank und Benutzer anlegen
  $con->query("CREATE DATABASE $database");
  $con->query("CREATE USER '$user'@'localhost' IDENTIFIED BY '$password'");
  $con->query("GRANT ALL PRIVILEGES ON $database.* TO '$user'@'localhost'");
  $con->query("FLUSH PRIVILEGES");
  $con->select_db($database);

  //Tabellen erstellen
  $a = $con->query(<<<SQL
CREATE TABLE `cards` (
  `id` int(11) NOT NULL AUTO_INCREMENT, 
  `form` text NOT NULL, 
  `value` text NOT NULL, 
  `status` int(11) NOT NULL, 
  `on_hand` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
  );
  $con->query(<<<SQL
CREATE TABLE `game` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL,
  `requested_shape` text,
  `draw_counter` int(11) NOT NULL,
  `mau_counter` int(11) NOT NULL,
  `turn_stage` int(11) NOT NULL COMMENT '0 = vor dem Zug; 1 = Karte gelegt; 2 = Farbe muss noch gewünscht werden; 3 = Karten gezogen',
  `turn` int(11) NOT NULL,
  `info` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
  );
  $con->query(<<<SQL
CREATE TABLE `players` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session_id` text,
  `name` text NOT NULL,
  `start_num` int(11) NOT NULL,
  `host` int(11) NOT NULL,
  `rank` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL
  );

  //Tabellen modifizieren
  $con->query(<<<SQL
ALTER TABLE `cards`
  ADD KEY `cards_ibfk_1` (`on_hand`);
SQL
  );
  $con->query(<<<SQL
ALTER TABLE `cards`
  ADD CONSTRAINT `cards_ibfk_1` FOREIGN KEY (`on_hand`) REFERENCES `players` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;
SQL
  );
  
  //Karten einfügen
  $con->query(<<<SQL
INSERT INTO cards (`id`, `form`, `value`, `status`) VALUES
  (1, 'Pi', '2', 0),(2, 'Pi', '3', 0),(3, 'Pi', '4', 0),(4, 'Pi', '5', 0),
  (5, 'Pi', '6', 0),(6, 'Pi', '7', 0),(7, 'Pi', '8', 0),(8, 'Pi', '9', 0),
  (9, 'Pi', 'X', 0),(10, 'Pi', 'B', 0),(11, 'Pi', 'D', 0),(12, 'Pi', 'K', 0),
  (13, 'Pi', 'A', 0),(14, 'Kr', '2', 0),(15, 'Kr', '3', 0),(16, 'Kr', '4', 0),
  (17, 'Kr', '5', 0),(18, 'Kr', '6', 0),(19, 'Kr', '7', 0),(20, 'Kr', '8', 0),
  (21, 'Kr', '9', 0),(22, 'Kr', 'X', 0),(23, 'Kr', 'B', 0),(24, 'Kr', 'D', 0),
  (25, 'Kr', 'K', 0),(26, 'Kr', 'A', 0),(27, 'He', '2', 0),(28, 'He', '3', 0),
  (29, 'He', '4', 0),(30, 'He', '5', 0),(31, 'He', '6', 0),(32, 'He', '7', 0),
  (33, 'He', '8', 0),(34, 'He', '9', 0),(35, 'He', 'X', 0),(36, 'He', 'B', 0),
  (37, 'He', 'D', 0),(38, 'He', 'K', 0),(39, 'He', 'A', 0),(40, 'Ka', '2', 0),
  (41, 'Ka', '3', 0),(42, 'Ka', '4', 0),(43, 'Ka', '5', 0),(44, 'Ka', '6', 0),
  (45, 'Ka', '7', 0),(46, 'Ka', '8', 0),(47, 'Ka', '9', 0),(48, 'Ka', 'X', 0),
  (49, 'Ka', 'B', 0),(50, 'Ka', 'D', 0),(51, 'Ka', 'K', 0),(52, 'Ka', 'A', 0);

SQL
  );
}
?>