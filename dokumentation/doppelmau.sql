-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 05. Apr 2019 um 10:57
-- Server-Version: 10.1.38-MariaDB
-- PHP-Version: 7.3.3

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `doppelmau`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `cards`
--

CREATE TABLE `cards` (
  `id` int(11) NOT NULL,
  `form` text NOT NULL,
  `value` text NOT NULL,
  `status` int(11) NOT NULL,
  `on_hand` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `cards`
--

INSERT INTO `cards` (`id`, `form`, `value`, `status`) VALUES
(1, 'Pi', '2', 0),
(2, 'Pi', '3', 0),
(3, 'Pi', '4', 0),
(4, 'Pi', '5', 0),
(5, 'Pi', '6', 0),
(6, 'Pi', '7', 0),
(7, 'Pi', '8', 0),
(8, 'Pi', '9', 0),
(9, 'Pi', 'X', 0),
(10, 'Pi', 'B', 0),
(11, 'Pi', 'D', 0),
(12, 'Pi', 'K', 0),
(13, 'Pi', 'A', 0),
(14, 'Kr', '2', 0),
(15, 'Kr', '3', 0),
(16, 'Kr', '4', 0),
(17, 'Kr', '5', 0),
(18, 'Kr', '6', 0),
(19, 'Kr', '7', 0),
(20, 'Kr', '8', 0),
(21, 'Kr', '9', 0),
(22, 'Kr', 'X', 0),
(23, 'Kr', 'B', 0),
(24, 'Kr', 'D', 0),
(25, 'Kr', 'K', 0),
(26, 'Kr', 'A', 0),
(27, 'He', '2', 0),
(28, 'He', '3', 0),
(29, 'He', '4', 0),
(30, 'He', '5', 0),
(31, 'He', '6', 0),
(32, 'He', '7', 0),
(33, 'He', '8', 0),
(34, 'He', '9', 0),
(35, 'He', 'X', 0),
(36, 'He', 'B', 0),
(37, 'He', 'D', 0),
(38, 'He', 'K', 0),
(39, 'He', 'A', 0),
(40, 'Ka', '2', 0),
(41, 'Ka', '3', 0),
(42, 'Ka', '4', 0),
(43, 'Ka', '5', 0),
(44, 'Ka', '6', 0),
(45, 'Ka', '7', 0),
(46, 'Ka', '8', 0),
(47, 'Ka', '9', 0),
(48, 'Ka', 'X', 0),
(49, 'Ka', 'B', 0),
(50, 'Ka', 'D', 0),
(51, 'Ka', 'K', 0),
(52, 'Ka', 'A', 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `game`
--

CREATE TABLE `game` (
  `id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `requested_shape` text,
  `draw_counter` int(11) NOT NULL,
  `mau_counter` int(11) NOT NULL,
  `turn_stage` int(11) NOT NULL COMMENT '0 = vor dem Zug; 1 = Karte gelegt; 2 = Farbe gewünscht; 3 = Karten gezogen',
  `turn` int(11) NOT NULL,
  `info` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `players`
--

CREATE TABLE `players` (
  `id` int(11) NOT NULL,
  `session_id` text,
  `name` text NOT NULL,
  `start_num` int(11) NOT NULL,
  `host` int(11) NOT NULL,
  `rank` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `cards`
--
ALTER TABLE `cards`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `cards_ibfk_1` (`on_hand`);

--
-- Indizes für die Tabelle `game`
--
ALTER TABLE `game`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `players`
--
ALTER TABLE `players`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `cards`
--
ALTER TABLE `cards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT für Tabelle `game`
--
ALTER TABLE `game`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `players`
--
ALTER TABLE `players`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `cards`
--
ALTER TABLE `cards`
  ADD CONSTRAINT `cards_ibfk_1` FOREIGN KEY (`on_hand`) REFERENCES `players` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
